# Cloud Resource Monitoring Dashboard

## Overview
This is a React-based application for monitoring cloud resources. It includes real-time notifications, data visualizations, and a responsive UI.

## Features
- **Dashboard Design**: Fully responsive layout with clean design.
- **Charts**: Line and bar charts for CPU and memory usage with tooltips.
- **Server Status**: Mock statuses for servers with dynamic coloring.
- **Real-Time Notifications**: Simulated alerts like server overloads.
- **Search**: Advanced filtering by attributes like status, name, and region.

## Getting Started
### Installation
1. Clone the repository:
   ```bash
   git clone <repo-url>
   cd cloud-dashboard

### Run local server
    npm install
    npm start
