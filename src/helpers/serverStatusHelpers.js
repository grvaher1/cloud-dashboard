export const mockServers = [
    { id: "server1", name: "Server A", status: "Active", region: "US-East" },
    { id: "server2", name: "Server B", status: "Down", region: "EU-West" },
    { id: "server3", name: "Server C", status: "Down", region: "AP-South" },
  ];
  
  export const mockServerDetails = {
    uptime: "124 days, 5 hours",
    cpuUsage: "56%",
    memoryUsage: "68%",
    lastChecked: "12:45 PM, Today",
    diskUsage: "450GB/1TB",
  };
  
  /**
   * Filters servers based on a search term.
   * @param {Array} servers - List of servers.
   * @param {string} searchTerm - Search term to filter by.
   * @returns {Array} Filtered servers.
   */
  export const filterServers = (servers, searchTerm) => {
    return servers.filter(
      (server) =>
        server.name.toLowerCase().includes(searchTerm.toLowerCase()) ||
        server.status.toLowerCase().includes(searchTerm.toLowerCase()) ||
        server.region.toLowerCase().includes(searchTerm.toLowerCase())
    );
  };
  
  /**
   * Simulates fetching server details.
   * @param {Object} server - Server for which details are fetched.
   * @returns {Promise<Object>} Server details.
   */
  export const fetchMockServerDetails = (server) => {
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve({ ...server, ...mockServerDetails });
      }, 2000);
    });
  };
  