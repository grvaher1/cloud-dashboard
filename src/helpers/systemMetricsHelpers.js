const commonOptions = {
    xAxis: { type: "datetime" },
    yAxis: {},
    series: [],
  };
  
  /**
   * Get options for CPU utilization chart
   * @param {Array} cpuData - Data for the CPU chart
   * @returns {Object} Chart options
   */
  export const getCpuOptions = (cpuData) => ({
    ...commonOptions,
    title: { text: "CPU Utilization (%)" },
    series: [{ name: "CPU", data: cpuData, color: "#096DD9" }],
    yAxis: { title: { text: "CPU Utilization (%)" } },
  });
  
  /**
   * Get options for Memory usage chart
   * @param {Array} memoryData - Data for the Memory chart
   * @returns {Object} Chart options
   */
  export const getMemoryOptions = (memoryData) => ({
    ...commonOptions,
    title: { text: "Memory Usage (GB)" },
    series: [{ name: "Memory", data: memoryData, color: "#096DD9" }],
    yAxis: { title: { text: "Memory Usage (GB)" } },
  });
  