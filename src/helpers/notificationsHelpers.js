/**
 * Generates a random notification object.
 * @returns {Object} Notification object with a unique id and message.
 */
export const generateRandomNotification = () => {
    const servers = ["Server A", "Server B", "Server C"];
    const randomServer = servers[Math.floor(Math.random() * servers.length)];
    return {
      id: Date.now(),
      message: `Issue detected at ${randomServer} at ${new Date().toLocaleTimeString()}`,
    };
  };
  