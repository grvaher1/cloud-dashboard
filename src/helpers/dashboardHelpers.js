/**
 * Generate mock data for CPU and Memory usage
 * @returns {Array} Mock data
 */
export const generateMockData = () => {
    return Array.from({ length: 10 }, (_, i) => ({
      x: Date.now() - i * 60000,
      y: Math.random() * 100,
    }));
  };
  
  /**
   * Fetch the name of a server based on its ID
   * @param {Array} servers - List of servers
   * @param {string} serverId - Selected server ID
   * @returns {string} Server name
   */
  export const getSelectedServerName = (servers, serverId) => {
    return servers.find((server) => server.id === serverId)?.name || "Unknown Server";
  };

  /**
 * Generate mock memory data by adding random values
 * @param {Array} data - Base data to transform
 * @returns {Array} Transformed memory data
 */
export const generateMockMemoryData = (data) => {
  return data.map((item) => ({
    ...item,
    y: Math.random() * 16,
  }));
};

  