export const SERVER_STATUS_TOOLTIP_TITLE = "View system metrics for the selected server";

export const SERVER_DETAILS = "Server Details";
