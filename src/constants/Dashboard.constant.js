export const mockServers = [
    { id: "server1", name: "Server A", status: "Active", region: "US-East" },
    { id: "server2", name: "Server B", status: "Down", region: "EU-West" },
    { id: "server3", name: "Server C", status: "Down", region: "AP-South" },
  ];
  