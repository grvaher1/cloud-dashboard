module.exports = {
    transform: {
      '^.+\\.(js|jsx)$': 'babel-jest',
    },
    testEnvironment: 'jsdom', // Ensures DOM-like environment for JSX
  };  