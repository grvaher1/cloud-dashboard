// __tests__/systemMetricsHelpers.test.js

import { getCpuOptions, getMemoryOptions } from "../helpers/systemMetricsHelpers";

describe("System Metrics Helpers", () => {
  const mockCpuData = [
    { x: 1629475200000, y: 35 },
    { x: 1629478800000, y: 40 },
    { x: 1629482400000, y: 45 },
  ];
  
  const mockMemoryData = [
    { x: 1629475200000, y: 8 },
    { x: 1629478800000, y: 10 },
    { x: 1629482400000, y: 12 },
  ];

  describe("getCpuOptions", () => {
    it("should return chart options with the correct structure for CPU data", () => {
      const options = getCpuOptions(mockCpuData);

      expect(options).toHaveProperty("title");
      expect(options.title.text).toBe("CPU Utilization (%)");

      expect(options).toHaveProperty("series");
      expect(options.series).toHaveLength(1);
      expect(options.series[0].name).toBe("CPU");
      expect(options.series[0].data).toEqual(mockCpuData);
      expect(options.series[0].color).toBe("#096DD9");

      expect(options).toHaveProperty("yAxis");
      expect(options.yAxis.title.text).toBe("CPU Utilization (%)");
    });
  });

  describe("getMemoryOptions", () => {
    it("should return chart options with the correct structure for Memory data", () => {
      const options = getMemoryOptions(mockMemoryData);

      expect(options).toHaveProperty("title");
      expect(options.title.text).toBe("Memory Usage (GB)");

      expect(options).toHaveProperty("series");
      expect(options.series).toHaveLength(1);
      expect(options.series[0].name).toBe("Memory");
      expect(options.series[0].data).toEqual(mockMemoryData);
      expect(options.series[0].color).toBe("#096DD9");

      expect(options).toHaveProperty("yAxis");
      expect(options.yAxis.title.text).toBe("Memory Usage (GB)");
    });
  });
});
