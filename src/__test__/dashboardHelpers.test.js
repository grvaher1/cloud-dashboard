import { getSelectedServerName } from '../helpers/dashboardHelpers';

describe('getSelectedServerName', () => {
  const mockServers = [
    { id: 'server1', name: 'Server A' },
    { id: 'server2', name: 'Server B' },
    { id: 'server3', name: 'Server C' },
  ];

  it('should return the correct server name for a valid ID', () => {
    const serverName = getSelectedServerName(mockServers, 'server1');
    expect(serverName).toBe('Server A');
  });

  it('should return "Unknown Server" for an invalid ID', () => {
    const serverName = getSelectedServerName(mockServers, 'invalidId');
    expect(serverName).toBe('Unknown Server');
  });

  it('should return "Unknown Server" if the servers array is empty', () => {
    const serverName = getSelectedServerName([], 'server1');
    expect(serverName).toBe('Unknown Server');
  });
});
