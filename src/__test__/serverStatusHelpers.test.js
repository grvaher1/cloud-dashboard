import { mockServers, mockServerDetails, filterServers, fetchMockServerDetails } from "../helpers/serverStatusHelpers";

describe("filterServers", () => {
  it("should return servers matching the search term in the name", () => {
    const result = filterServers(mockServers, "Server A");
    expect(result).toHaveLength(1);
    expect(result[0].name).toBe("Server A");
  });

  it("should return servers matching the search term in the status", () => {
    const result = filterServers(mockServers, "Down");
    expect(result).toHaveLength(2);
    expect(result[0].status).toBe("Down");
    expect(result[1].status).toBe("Down");
  });

  it("should return servers matching the search term in the region", () => {
    const result = filterServers(mockServers, "US-East");
    expect(result).toHaveLength(1);
    expect(result[0].region).toBe("US-East");
  });

  it("should return an empty array if no servers match the search term", () => {
    const result = filterServers(mockServers, "Nonexistent");
    expect(result).toHaveLength(0);
  });

  it("should be case insensitive", () => {
    const result = filterServers(mockServers, "server a");
    expect(result).toHaveLength(1);
    expect(result[0].name).toBe("Server A");
  });
});

describe("fetchMockServerDetails", () => {
  it("should resolve with mock server details merged with the input server", async () => {
    const server = mockServers[0];
    const result = await fetchMockServerDetails(server);
    expect(result).toEqual({ ...server, ...mockServerDetails });
  });

  it("should resolve after a delay", async () => {
    const server = mockServers[0];
    const startTime = Date.now();

    await fetchMockServerDetails(server);

    const endTime = Date.now();
    expect(endTime - startTime).toBeGreaterThanOrEqual(2000);
  });
});
