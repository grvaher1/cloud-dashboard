import React, { useState, useEffect, useCallback } from "react";
import {
  Card,
  Typography,
  Input,
  Button,
  Modal,
  Skeleton,
  Row,
  Tooltip,
} from "antd";
import {
  CheckCircleOutlined,
  ClockCircleOutlined,
  DatabaseOutlined,
  DesktopOutlined,
  InfoCircleOutlined,
} from "@ant-design/icons";
import {
  filterServers,
  fetchMockServerDetails,
  mockServers,
} from "../../helpers/serverStatusHelpers";
import {
  SERVER_STATUS_TOOLTIP_TITLE,
  SERVER_DETAILS,
} from '../../constants/ServerStatus.constant'
import "./ServerStatus.css";

const ServerStatus = ({ servers = mockServers }) => {
  const [open, setOpen] = useState(false);
  const [loading, setLoading] = useState(false);
  const [serverDetails, setServerDetails] = useState(null);
  const [searchTerm, setSearchTerm] = useState("");
  const [filteredServers, setFilteredServers] = useState(servers);

  useEffect(() => {
    setFilteredServers(filterServers(servers, searchTerm));
  }, [searchTerm, servers]);

  const handleOpen = useCallback((server) => {
    setServerDetails(null);
    setLoading(true);
    setOpen(true);

    fetchMockServerDetails(server).then((details) => {
      setServerDetails(details);
      setLoading(false);
    });
  }, []);

  const handleClose = useCallback(() => {
    setOpen(false);
  }, []);

  const handleSearchChange = useCallback((e) => {
    setSearchTerm(e.target.value);
  }, []);

  return (
    <>
      <Card
        title={
          <div className="server-status-title">
            <Typography.Title level={4}>
              Server Status
              <Tooltip title={SERVER_STATUS_TOOLTIP_TITLE}>
                <Button
                  shape="circle"
                  icon={<InfoCircleOutlined />}
                  size="small"
                  className="tooltip-button"
                />
              </Tooltip>
            </Typography.Title>
          </div>
        }
        className="server-card"
      >
        <Input.Search
          placeholder="Search Servers"
          allowClear
          enterButton
          value={searchTerm}
          onChange={handleSearchChange}
          className="server-search"
        />
        {filteredServers.length > 0 ? (
          filteredServers.map((server) => (
            <Row
              key={server.id}
              justify="space-between"
              align="middle"
              className="server-row"
            >
              <Typography.Text className="server-row-text">
                <strong>{server.name}</strong> - {server.status} ({server.region})
              </Typography.Text>
              <Button
                type="primary"
                onClick={() => handleOpen(server)}
                icon={<InfoCircleOutlined />}
                className="view-details-button"
              >
                View Details
              </Button>
            </Row>
          ))
        ) : (
          <Typography.Text className="no-servers">
            No matching servers found
          </Typography.Text>
        )}
      </Card>
      <Modal
        title={serverDetails?.name || SERVER_DETAILS}
        visible={open}
        onCancel={handleClose}
        footer={[
          <Button key="close" onClick={handleClose}>
            Close
          </Button>,
        ]}
        className="server-modal"
      >
        {loading ? (
          <Skeleton active paragraph={{ rows: 5 }} />
        ) : (
          <>
            <Row className="modal-row" align="middle">
              <CheckCircleOutlined className="modal-icon success-icon" />
              <Typography.Text>
                <strong>Status:</strong> {serverDetails?.status}
              </Typography.Text>
            </Row>
            <Row className="modal-row" align="middle">
              <ClockCircleOutlined className="modal-icon warning-icon" />
              <Typography.Text>
                <strong>Uptime:</strong> {serverDetails?.uptime}
              </Typography.Text>
            </Row>
            <Row className="modal-row" align="middle">
              <DesktopOutlined className="modal-icon info-icon" />
              <Typography.Text>
                <strong>CPU Usage:</strong> {serverDetails?.cpuUsage}
              </Typography.Text>
            </Row>
            <Row className="modal-row" align="middle">
              <DatabaseOutlined className="modal-icon memory-icon" />
              <Typography.Text>
                <strong>Memory Usage:</strong> {serverDetails?.memoryUsage}
              </Typography.Text>
            </Row>
            <Row className="modal-row" align="middle">
              <DatabaseOutlined className="modal-icon disk-icon" />
              <Typography.Text>
                <strong>Disk Usage:</strong> {serverDetails?.diskUsage}
              </Typography.Text>
            </Row>
          </>
        )}
      </Modal>
    </>
  );
};

export default React.memo(ServerStatus);
