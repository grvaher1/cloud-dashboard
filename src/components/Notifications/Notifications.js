import React, { useState, useEffect, useCallback } from "react";
import { Typography } from "antd";
import { FixedSizeList as List } from "react-window";
import { CloseCircleOutlined, WarningOutlined } from "@ant-design/icons";
import { generateRandomNotification } from "../../helpers/notificationsHelpers";
import "./Notifications.css";

const Notifications = () => {
  const [notifications, setNotifications] = useState([]);

  useEffect(() => {
    const interval = setInterval(() => {
      const newNotification = generateRandomNotification();
      setNotifications((prev) => [...prev, newNotification]);
    }, 5000);

    return () => clearInterval(interval);
  }, []);

  const dismissNotification = useCallback((id) => {
    setNotifications((prev) => prev.filter((notif) => notif.id !== id));
  }, []);

  const Row = useCallback(
    ({ index, style }) => {
      const notification = notifications[index];
      return (
        <div style={style} className="notification-row">
          <WarningOutlined className="warning-icon" />
          <Typography.Text className="notification-message">
            {notification.message}
          </Typography.Text>
          <CloseCircleOutlined
            onClick={() => dismissNotification(notification.id)}
            className="dismiss-icon"
          />
        </div>
      );
    },
    [notifications, dismissNotification]
  );

  return (
    <div
      className="notification-container"
    >
      {notifications?.length > 0 ? (
        <List
          height={300}
          itemCount={notifications.length}
          itemSize={50}
          width="100%"
        >
          {Row}
        </List>
      ) : (
        <Typography.Text className="no-notifications">
          No Notifications
        </Typography.Text>
      )}
    </div>
  );
};

export default React.memo(Notifications);