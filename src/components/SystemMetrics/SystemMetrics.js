import React, { useMemo } from "react";
import Highcharts from "highcharts";
import HighchartsReact from "highcharts-react-official";
import { Card, Tabs } from "antd";
import { getCpuOptions, getMemoryOptions } from "../../helpers/systemMetricsHelpers";
import "./SystemMetrics.css";

const { TabPane } = Tabs;

const SystemMetrics = ({ cpuData, memoryData }) => {
  const cpuOptions = useMemo(() => getCpuOptions(cpuData), [cpuData]);
  const memoryOptions = useMemo(() => getMemoryOptions(memoryData), [memoryData]);

  return (
    <Card>
      <Tabs defaultActiveKey="1">
        <TabPane tab="CPU" key="1">
          <HighchartsReact highcharts={Highcharts} options={cpuOptions} />
        </TabPane>
        <TabPane tab="Memory" key="2">
          <HighchartsReact highcharts={Highcharts} options={memoryOptions} />
        </TabPane>
      </Tabs>
    </Card>
  );
};

export default React.memo(SystemMetrics);