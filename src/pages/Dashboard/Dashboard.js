import React, { useState, useEffect } from "react";
import { Layout, Row, Col } from "antd";
import Header from "./Sections/DashboardHeader";
import ServerStatusSection from "./Sections/ServerStatusSection";
import NotificationsSection from "./Sections/NotificationsSection";
import SystemMetricsSection from "./Sections/SystemMetricsSection";
import { generateMockData, generateMockMemoryData } from "../../helpers/dashboardHelpers";
import { mockServers } from "../../constants/Dashboard.constant";
import "./Dashboard.css";

const { Content } = Layout;

const Dashboard = () => {
  const [cpuData, setCpuData] = useState([]);
  const [memoryData, setMemoryData] = useState([]);
  const [selectedServerId, setSelectedServerId] = useState(mockServers[0].id);

  useEffect(() => {
    setCpuData(generateMockData());
    setMemoryData(generateMockMemoryData(generateMockData()));
  }, [selectedServerId]);

  return (
    <Layout>
      <Header />
      <Content className="container">
        <Row gutter={[16, 16]}>
          <Col xs={24} md={16}>
            <ServerStatusSection servers={mockServers} />
          </Col>
          <Col xs={24} md={8}>
            <NotificationsSection />
          </Col>
          <Col xs={24}>
            <SystemMetricsSection
              cpuData={cpuData}
              memoryData={memoryData}
              servers={mockServers}
              selectedServerId={selectedServerId}
              setSelectedServerId={setSelectedServerId}
            />
          </Col>
        </Row>
      </Content>
    </Layout>
  );
};

export default Dashboard;
