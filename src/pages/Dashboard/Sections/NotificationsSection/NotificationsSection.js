import React, { memo } from "react";
import { Typography, Tooltip, Button } from "antd";
import { InfoCircleOutlined } from "@ant-design/icons";
import Notifications from "../../../../components/Notifications";
import { NOTIFICATIONS_TOOLTIP_TITLE } from '../../../../constants/Notifications.constants';
import "./NotificationsSection.css";

const NotificationsSection = () => {
  return (
    <div className="notifications-section">
      <Typography.Title level={4} className="section-title">
        Notifications
        <Tooltip title={NOTIFICATIONS_TOOLTIP_TITLE}>
          <Button
            shape="circle"
            icon={<InfoCircleOutlined />}
            size="small"
            className="tooltip-button"
          />
        </Tooltip>
      </Typography.Title>
      <div className="notification-box">
        <Notifications />
      </div>
    </div>
  );
};

export default memo(NotificationsSection);
