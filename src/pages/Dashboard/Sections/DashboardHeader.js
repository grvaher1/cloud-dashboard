import React from "react";
import { Layout, Typography } from "antd";
import "./DashboardHeader.css";

const { Header } = Layout;

const DashboardHeader = () => {
  return (
    <Header className="header">
      <Typography.Title level={2} className="dashboard-title">
        Cloud Dashboard
      </Typography.Title>
    </Header>
  );
};

export default DashboardHeader;
