import React, { useMemo, useCallback } from "react";
import { Typography, Tooltip, Button, Select } from "antd";
import { InfoCircleOutlined } from "@ant-design/icons";
import SystemMetrics from "../../../../components/SystemMetrics";
import { SYSTEM_METRICS_TOOLTIP_TITLE } from "../../../../constants/SystemMetricsSection.constant";
import "./SystemMetricsSection.css";

const { Option } = Select;

const SystemMetricsSection = ({
  cpuData,
  memoryData,
  servers,
  selectedServerId,
  setSelectedServerId,
}) => {
  const selectedServer = useMemo(
    () => servers.find((server) => server.id === selectedServerId)?.name,
    [servers, selectedServerId]
  );

  const serverOptions = useMemo(
    () =>
      servers.map((server) => (
        <Option key={server.id} value={server.id}>
          {server.name}
        </Option>
      )),
    [servers]
  );

  const handleServerChange = useCallback(
    (value) => setSelectedServerId(value),
    [setSelectedServerId]
  );

  return (
    <div className="system-metrics-section">
      <Typography.Title level={4} className="section-title">
        System Metrics
        <Tooltip title={SYSTEM_METRICS_TOOLTIP_TITLE}>
          <Button
            shape="circle"
            icon={<InfoCircleOutlined />}
            size="small"
            className="tooltip-button"
          />
        </Tooltip>
      </Typography.Title>
      <Select
        value={selectedServerId}
        onChange={handleServerChange}
        className="server-select"
      >
        {serverOptions}
      </Select>
      <SystemMetrics
        cpuData={cpuData}
        memoryData={memoryData}
        selectedServer={selectedServer}
      />
    </div>
  );
};

export default React.memo(SystemMetricsSection);
