import React from "react";
import ServerStatus from "../../../../components/ServerStatus";
import "./ServerStatusSection.css";

const ServerStatusSection = ({ servers }) => {
  return (
    <div className="server-status-section">
      <ServerStatus servers={servers} />
    </div>
  );
};

export default React.memo(ServerStatusSection);